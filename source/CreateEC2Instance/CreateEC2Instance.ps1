﻿#
# Script to create an AWS EC2 instance.
#
#      Create 2015-08-05 By Masato Kobayashi
#
#
#   Copyright (C) 2015 Masato Kobayashi. All rights reserved.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

<#
   To specify a profile to use.
   Default: default

   Add a new profile :
     PS C:\> Set-AWSCredentials -AccessKey AKIAIOSFODNN7EXAMPLE -SecretKey wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY -StoreAs ProfileName
 #>
$ProfileName = $null


# Constant declaration
Set-Variable -name SUCCESS -value 0 -option constant
Set-Variable -name FATAL   -value 1 -option constant

<#
   This parameter overrides confirmation prompts to force the cmdlet to continue its 
   operation. This parameter should always be used with caution.
 #>
$Force = $false

<#
   The block device mapping for the instance. For more information, see Block Device Mapping 
   (http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSEncryption.html) in the 
   Amazon Elastic Compute Cloud User Guide.

   DeviceName : The device name exposed to the instance (for example, /dev/sdh or xvdh ).

   VirtualName : The virtual device name ( ephemeral N ). Instance store volumes are numbered 
                 starting from 0. An instance type with 2 available instance store volumes 
                 can specify mappings for ephemeral0 and ephemeral1 .
                 The number of available instance store volumes depends on the instance type. 
                 After you connect to the instance, you must mount the volume.
                 Constraints: For M3 instances, you must specify instance store volumes in 
                              the block device mapping for the instance. When you launch an 
                              M3 instance, we ignore any instance store volumes specified in 
                              the block device mapping for the AMI.

   NoDevice : Suppresses the specified device included in the block device mapping of the AMI.

   Ebs : Parameters used to automatically set up EBS volumes when the instance is launched.

            DeleteOnTermination : Indicates whether the EBS volume is deleted on instance termination.

            Encrypted : Indicates whether the EBS volume is encrypted. Encrypted Amazon EBS volumes 
                        may only be attached to instances that support Amazon EBS encryption.

            Iops : The number of I/O operations per second (IOPS) that the volume supports. For 
                   Provisioned IOPS (SSD) volumes, this represents the number of IOPS that are 
                   provisioned for the volume. For General Purpose (SSD) volumes, this represents the 
                   baseline performance of the volume and the rate at which the volume accumulates 
                   I/O credits for bursting. For more information on General Purpose (SSD) baseline 
                   performance, I/O credits, and bursting, see Amazon EBS Volume Types in the Amazon 
                   Elastic Compute Cloud User Guide.
                   Constraint: Range is 100 to 20000 for Provisioned IOPS (SSD) volumes and 3 to 10000 
                               for General Purpose (SSD) volumes.
                   Condition: This parameter is required for requests to create io1 volumes; it is not 
                              used in requests to create standard or gp2 volumes.

            SnapshotId : The ID of the snapshot.

            VolumeSize : The size of the volume, in GiB.
                         Constraints: 1-1024 for standard volumes,
                                      1-16384 for gp2 volumes, and
                                      4-16384 for io1 volumes. If you specify a snapshot, the volume size 
                                      must be equal to or larger than the snapshot size.
                         Default: If you're creating the volume from a snapshot and don't specify a volume 
                                  size, the default is the snapshot size.

            VolumeType : The volume type.
                         gp2 for General Purpose (SSD) volumes,
                         io1 for Provisioned IOPS (SSD) volumes, and
                         standard for Magnetic volumes.
                         Default: standard
 #>
$DeviceMappings = @(
    @{
        DeviceName  = '/dev/xvda'
        VirtualName = $null
        NoDevice    = $null
        Ebs         = @{
            DeleteOnTermination = $true
            Encrypted           = $false
            Iops                = $null
            SnapshotId          = $null
            VolumeSize          = 8
            VolumeType          = 'Gp2'
        
        }
    }
)



<#
   The ID of the AMI to launch. The set of available AMI IDs can be determined using 
   the Get-EC2Image or Get-EC2ImageByName cmdlets.
   Default: Amazon Linux AMI
 #>
$ImageId = $null
<#
   The minimum number of instances to launch. If you specify a minimum that is more 
   instances than Amazon EC2 can launch in the target Availability Zone, Amazon EC2 
   launches no instances. Constraints: Between 1 and the maximum number you're 
   allowed for the specified instance type. For more information about the default 
   limits, and how to request an increase, see How many instances can I run in 
   Amazon EC2 in the Amazon EC2 General FAQ. 
   Default: 1.
 #>
$MinCount = 1
<#
   The maximum number of instances to launch. If you specify a maximum that is more 
   instances than Amazon EC2 can launch in the target Availability Zone EC2 will 
   try to launch the maximum number for the target Availability Zone, but launches 
   no fewer than the minimum number. Constraints: Between 1 and the maximum number 
   you're allowed for the specified instance type. For more information about the 
   default limits, and how to request an increase, see How many instances can I 
   run in Amazon EC2 in the Amazon EC2 General FAQ. 
   Default: 1.
 #>
$MaxCount = 1
<#
   The name of the key pair to use to connect to the instance using remote desktop or SSH.
 #>
$KeyName = "AWS-Linux_20140421_1130"
<#
   The names of one or more security groups. Note that for a nondefault VPC, you must 
   specify the security group by ID using the SecurityGroupIds parameter instead. For 
   EC2-Classic or a default VPC, you can specify the security group by name or ID.
   Default: Amazon EC2 uses the default security group
 #>
$SecurityGroup = $null
<#
   The IDs of one or more security groups. Note that for a nondefault VPC, you must 
   specify the security group by ID using this parameter. For EC2-Classic or a default 
   VPC, you can specify the security group by name or ID.
 #>
$SecurityGroupId = @( 'sg-2d7c8d48' )
<#
   The base64-encoded MIME user data for the instances. If the -EncodeUserData switch 
   is also set, the value for this parameter can be supplied as normal ASCII text 
   and will be base64-encoded by the cmdlet.
 #>
$UserData = @'
#!/bin/bash -ex
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
yum -y install gcc gcc-c++ make yum-fastestmirror git
yum -y install ruby rubygems ruby-devel openssl-devel
/usr/bin/gem install chef --no-ri --no-rdoc
/usr/bin/gem install ruby-shadow --no-ri --no-rdoc
/usr/bin/gem update
yum -y update
/bin/mv /etc/localtime /etc/localtime.ORG
/bin/cp -p /usr/share/zoneinfo/Japan /etc/localtime
echo ZONE="Asia/Tokyo" >  /etc/sysconfig/clock
echo UTC=false         >> /etc/sysconfig/clock
if [ ! -d /root/.ssh ]; then
    mkdir /root/.ssh
    /bin/chmod go-rwx /root/.ssh
fi
/usr/bin/aws s3 cp s3://concealment/AWS_Settings_deployment_Private.pem /root/.ssh/AWS_Settings_deployment_Private.pem
/bin/chmod go-rwx /root/.ssh/AWS_Settings_deployment_Private.pem
/usr/bin/aws s3 cp s3://concealment/known_hosts /root/.ssh/known_hosts
/usr/bin/aws s3 cp s3://concealment/config /root/.ssh/config
/bin/chmod go-rwx /root/.ssh/config
mkdir -p /tmp/setup
/usr/bin/git archive --remote=git@bitbucket.org:m-kobayashi/aws-settings.git --format=tar HEAD source/RegisterToRoute53 | tar -C /tmp/setup -xf -
mkdir -p /etc/aws
/bin/mv /tmp/setup/source/RegisterToRoute53/ddnsrecord.tmpl /etc/aws/ddnsrecord.tmpl
/bin/mv /tmp/setup/source/RegisterToRoute53/route53insert.sh /etc/rc.d/init.d/route53insert.sh
/bin/chmod +x /etc/rc.d/init.d/route53insert.sh
echo '' >> /etc/rc.d/rc.local
echo 'AWSLOG=/var/log/awsseting.log' >> /etc/rc.d/rc.local
echo "/bin/echo -n 'AWS Add A record to Route53. ' >> \${AWSLOG}" >> /etc/rc.d/rc.local
echo "date +\"%Y-%m-%d %H:%M:%S\" >> \${AWSLOG}" >> /etc/rc.d/rc.local
echo '/etc/rc.d/init.d/route53insert.sh >> ${AWSLOG} 2>&1' >> /etc/rc.d/rc.local
/etc/rc.d/init.d/route53insert.sh
'@
<#
   The name of a file containing base64-encoded MIME user data for the instances. 
   Using this parameter causes any value for the UserData parameter to be ignored. 
   If the -EncodeUserData switch is also set, the contents of the file can be normal 
   ASCII text and will be base64-encoded by the cmdlet.
 #>
$UserDataFile = ""
<#
   If set and the -UserData or -UserDataFile parameters are specified, the specified 
   user data is base64 encoded prior to submitting to EC2. By default the user data 
   is assumed to be encoded prior to being supplied to the cmdlet.
 #>
$EncodeUserData = $false
<#
   The instance type. For more information, 
   see Instance Types ( http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-types.html )
   in the Amazon Elastic Compute Cloud User Guide. 
   Valid values: t2.micro | t2.small | t2.medium | m3.medium | m3.large | m3.xlarge | m3.2xlarge | 
                 m1.small | m1.medium | m1.large | m1.xlarge | c3.large | c3.xlarge | c3.2xlarge | 
                 c3.4xlarge | c3.8xlarge | c1.medium | c1.xlarge | cc2.8xlarge | r3.large | 
                 r3.xlarge | r3.2xlarge | r3.4xlarge | r3.8xlarge | m2.xlarge | m2.2xlarge | 
                 m2.4xlarge | cr1.8xlarge | i2.xlarge | i2.2xlarge | i2.4xlarge | i2.8xlarge | 
                 hs1.8xlarge | hi1.4xlarge | t1.micro | g2.2xlarge | cg1.4xlarge
   Default: Amazon EC2 will use an m1.small instance if not specified.
 #>
$InstanceType = "t2.micro"
<#
   The Availability Zone in which to create the volume. Use DescribeAvailabilityZones 
   to list the Availability Zones that are currently available to you.
   Ext. : ap-northeast-1a | ap-northeast-1c
 #>
$AvailabilityZone = $null
<#
   The name of an existing placement group.
 #>
$PlacementGroup = $null
<#
   The tenancy of the instance. An instance with a tenancy of dedicated runs on 
   single-tenant hardware and can only be launched into a VPC.
   Valid Values: default | dedicated
 #>
$Tenancy = $null
<#
   The ID of the kernel for the instance.
   Important: We recommend that you use PV-GRUB instead of kernels and RAM disks. 
              For more information, see PV-GRUB in the Amazon Elastic Compute Cloud User Guide.
 #>
$KernelId = $null
<#
   The ID of the RAM disk.
   Important: We recommend that you use PV-GRUB instead of kernels and RAM disks.
              For more information, see PV-GRUB in the Amazon Elastic Compute Cloud User Guide.
 #>
$RamdiskId = $null
<#
   The block device mapping for the instance. For more information, see Block Device Mapping 
   ( http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSEncryption.html ) in the Amazon 
   Elastic Compute Cloud User Guide.
 #>
$BlockDeviceMapping = $null
<#
   Enables monitoring for the instance.
 #>
$Monitoring_Enabled = $false
<#
   [EC2-VPC] The ID of the subnet to launch the instance into.
 #>
$SubnetId = $null
<#
   If you enable this option, you can't terminate the instance using the Amazon EC2 console, 
   CLI, or API; otherwise, you can. If you specify this option and then later want to be able 
   to terminate the instance, you must first change the value of the disableApiTermination 
   attribute to false using Edit-EC2InstanceAttribute. Alternatively, if you 
   set -InstanceInitiatedShutdownBehavior to 'terminate', you can terminate the instance by running 
   the shutdown command from the instance.
 #>
$DisableApiTermination = $false
<#
   Indicates whether an instance stops or terminates when you initiate shutdown from the instance 
   (using the operating system command for system shutdown).
   Valid values: stop | terminate. 
   Default: stop.
 #>
$InstanceInitiatedShutdownBehavior = $null
<#
   Unique, case-sensitive identifier you provide to ensure idempotency of the request. For more 
   information, see How to Ensure Idempotency in the Amazon Elastic Compute Cloud User Guide. 
   Constraints: Maximum 64 ASCII characters
 #>
$ClientToken = $null
<#
   A set of one or more existing network interfaces to attach to the instance.

   AssociatePublicIp : AssociatePublicIpAddress.
                       Indicates whether to assign a public IP address to an instance you launch in a VPC. 
                       The public IP address can only be assigned to a network interface for eth0, 
                       and can only be assigned to a new network interface, not an existing one. You 
                       cannot specify more than one network interface in the request. If launching 
                       into a default subnet, the default value is true

   DeleteOnTermination : If set to true , the interface is deleted when the instance is terminated. You 
                         can specify true only if creating a new network interface when launching an 
                         instance.

   SecurityGroupId : The IDs of the security groups for the network interface. Applies only if creating 
                     a network interface when launching an instance.

   PrivateIpAddress : The private IP address of the network interface. Applies only if creating a network 
                      interface when launching an instance.
 #>
$NetInterface = @(
    @{
        AssociatePublicIp = $true
        DeleteOnTermination = $true
        SecurityGroupId = $null
        PrivateIpAddress = $null
    }
)
<#
   Enables Amazon EBS optimization for the instance. This optimization provides dedicated throughput 
   to Amazon EBS and an optimized configuration stack to provide optimal Amazon EBS I/O performance. 
   This option isn't available with all instance types. Additional usage charge apply when using 
   this option.
   Default: false (disabled)
 #>
$EbsOptimized = $false
<#
   The ARN of an IAM instance profile to associate with the instances.
 #>
$InstanceProfile_Arn = $null
<#
   The name of an IAM instance profile to associate with the instances.
 #>
$InstanceProfile_Name = 'Route53RegisterRole'
<#
   Reserved for internal use.
 #>
$AdditionalInfo = $null

$Tags = @(
    @{ Key = 'Name';    Val = 'UserDate TEST' }
    @{ Key = 'domain';  Val = 'm-kobayashi.org' }
    @{ Key = 'host';    Val = 'test01' }
    @{ Key = 'ticket';  Val = '' }
    @{ Key = 'owner';   Val = '' }
    @{ Key = 'project'; Val = '' }
    @{ Key = 'stack';   Val = 'devel' }
)

import-module "C:\Program Files (x86)\AWS Tools\PowerShell\AWSPowerShell\AWSPowerShell.psd1"

# Check the required information
if ( !$NetInterface -or $NetInterface.Length -le 0 )
{
    Write-Host "Network interface has not been specified."
    exit $FATAL
}

# Change profile
if ( $ProfileName )
{
    try
    {
        Set-AWSCredentials -ProfileName $ProfileName
    }
    catch
    {
        Write-Host "It failed to change the profile. : $ProfileName"
        Write-Host $Error
        exit $FATAL
    }
}

# Create a list of devices.
$BlockDeviceMapping = @()
for ( $idx = 0; $idx -lt $DeviceMappings.Length; $idx++ )
{
    if ( $null -ne $DeviceMappings[$idx].DeviceName -and '' -ne $DeviceMappings[$idx].DeviceName )
    {
        $Mapping = New-Object Amazon.EC2.Model.BlockDeviceMapping
        $Mapping.DeviceName  = if ( $null -ne $DeviceMappings[$idx].DeviceName ) { $DeviceMappings[$idx].DeviceName } else { $null }
        $Mapping.NoDevice    = if ( $null -ne $DeviceMappings[$idx].NoDevice ) { $DeviceMappings[$idx].NoDevice } else { $null }
        $Mapping.VirtualName = $null
        $Device = New-Object Amazon.EC2.Model.EbsBlockDevice
        $Device.DeleteOnTermination = if ( $null -ne $DeviceMappings[$idx].Ebs.DeleteOnTermination ) { $DeviceMappings[$idx].Ebs.DeleteOnTermination } else { $null }
        if ( $null -ne $DeviceMappings[$idx].Ebs.Encrypted -and $DeviceMappings[$idx].DeviceName –match '[^Aa]\Z' )
        {
            $Device.Encrypted       = $DeviceMappings[$idx].Ebs.Encrypted
        }
        if ( $null -ne $DeviceMappings[$idx].Ebs.Iops -and $null -ne $DeviceMappings[$idx].Ebs.VolumeType -and 'io1' -eq $DeviceMappings[$idx].Ebs.VolumeType.ToLower() )
        {
            $Device.Iops            =  $DeviceMappings[$idx].Ebs.Iops
        }
        if ( $null -ne $DeviceMappings[$idx].Ebs.SnapshotId )
        {
            $Device.SnapshotId      = $DeviceMappings[$idx].Ebs.SnapshotId
        }
        $Device.VolumeSize          = if ( $null -ne $DeviceMappings[$idx].Ebs.VolumeSize ) { $DeviceMappings[$idx].Ebs.VolumeSize } else { $null }
        $Device.VolumeType          = if ( $null -ne $DeviceMappings[$idx].Ebs.VolumeType ) { New-Object Amazon.EC2.VolumeType( $DeviceMappings[$idx].Ebs.VolumeType ) } else { $null }
        $Mapping.Ebs         = $Device

        $BlockDeviceMapping += $Mapping
    }
    else { if ( $null -ne $DeviceMappings[$idx].VirtualName -and '' -ne $DeviceMappings[$idx].VirtualName )
    {
        $Mapping = New-Object Amazon.EC2.Model.BlockDeviceMapping
        $Mapping.DeviceName  = $null
        $Mapping.NoDevice    = $null
        $Mapping.VirtualName = $DeviceMappings[$idx].VirtualName
        $Mapping.Ebs         = $null

        $BlockDeviceMapping += $Mapping
    } }
}

# Create a list of network interface.
$NetworkInterface = @()
for ( $idx = 0; $idx -lt $NetworkInterface.Length; $idx++ )
{
    $Interface = New-Object Amazon.EC2.Model.InstanceNetworkInterfaceSpecification
    $Interface.DeviceIndex = $idx
    $Interface.AssociatePublicIpAddress = $NetInterface[$idx].AssociatePublicIp
    $Interface.DeleteOnTermination = $NetInterface[$idx].DeleteOnTermination
    if ( $null -ne $NetInterface[$idx].SecurityGroupId -and 0 -gt $NetInterface[$idx].SecurityGroupId.Length )
    {
        $Interface.Groups = $NetInterface[$idx].SecurityGroupId
    }

    $Interface.PrivateIpAddress = $NetInterface[$idx].PrivateIpAddress

    $NetworkInterface += $Interface
}

# Find the default AMI.
if ( !$ImageId )
{
    # Find the default Amazon Linux image.
    $ECImage = Get-EC2Image -Filter @{ Name="name"; Values="amzn-ami-hvm-????.??.?.x86_64-gp2" } | Sort-Object -descending CreationDate
    if ( $ECImage.Length )
    {
        $ImageId = $ECImage[0].ImageId
    }
}

# Create Instance.
try
{
    if ( !$EncodeUserData )
    {
        if ( !$Force )
        {
            $EC2Instance = New-EC2Instance -ImageId $ImageId -MinCount $MinCount -MaxCount $MaxCount `
                                           -SecurityGroup $SecurityGroup -SecurityGroupId $SecurityGroupId -KeyName $KeyName -UserData $UserData `
                                           -UserDataFile $UserDataFile -EncodeUserData -InstanceType $InstanceType -AvailabilityZone $AvailabilityZone `
                                           -PlacementGroup $PlacementGroup -Tenancy $Tenancy -KernelId $KernelId -RamdiskId $RamdiskId `
                                           -BlockDeviceMapping $BlockDeviceMapping -Monitoring_Enabled $Monitoring_Enabled -SubnetId $SubnetId `
                                           -DisableApiTermination $DisableApiTermination -InstanceInitiatedShutdownBehavior $InstanceInitiatedShutdownBehavior `
                                           -ClientToken $ClientToken -NetworkInterface $NetworkInterface `
                                           -EbsOptimized $EbsOptimized -InstanceProfile_Arn $InstanceProfile_Arn -InstanceProfile_Name $InstanceProfile_Name `
                                           -AdditionalInfo $AdditionalInfo -Force 
        }
        else
        {
            $EC2Instance = New-EC2Instance -ImageId $ImageId -MinCount $MinCount -MaxCount $MaxCount `
                                           -SecurityGroup $SecurityGroup -SecurityGroupId $SecurityGroupId -KeyName $KeyName -UserData $UserData `
                                           -UserDataFile $UserDataFile -EncodeUserData -InstanceType $InstanceType -AvailabilityZone $AvailabilityZone `
                                           -PlacementGroup $PlacementGroup -Tenancy $Tenancy -KernelId $KernelId -RamdiskId $RamdiskId `
                                           -BlockDeviceMapping $BlockDeviceMapping -Monitoring_Enabled $Monitoring_Enabled -SubnetId $SubnetId `
                                           -DisableApiTermination $DisableApiTermination -InstanceInitiatedShutdownBehavior $InstanceInitiatedShutdownBehavior `
                                           -ClientToken $ClientToken -NetworkInterface $NetworkInterface `
                                           -EbsOptimized $EbsOptimized -InstanceProfile_Arn $InstanceProfile_Arn -InstanceProfile_Name $InstanceProfile_Name `
                                           -AdditionalInfo $AdditionalInfo 
        }
    }
    else
    {
        if ( !$Force )
        {
            $EC2Instance = New-EC2Instance -ImageId $ImageId -MinCount $MinCount -MaxCount $MaxCount `
                                           -SecurityGroup $SecurityGroup -SecurityGroupId $SecurityGroupId -KeyName $KeyName -UserData $UserData `
                                           -UserDataFile $UserDataFile -InstanceType $InstanceType -AvailabilityZone $AvailabilityZone `
                                           -PlacementGroup $PlacementGroup -Tenancy $Tenancy -KernelId $KernelId -RamdiskId $RamdiskId `
                                           -BlockDeviceMapping $BlockDeviceMapping -Monitoring_Enabled $Monitoring_Enabled -SubnetId $SubnetId `
                                           -DisableApiTermination $DisableApiTermination -InstanceInitiatedShutdownBehavior $InstanceInitiatedShutdownBehavior `
                                           -ClientToken $ClientToken -NetworkInterface $NetworkInterface `
                                           -EbsOptimized $EbsOptimized -InstanceProfile_Arn $InstanceProfile_Arn -InstanceProfile_Name $InstanceProfile_Name `
                                           -AdditionalInfo $AdditionalInfo -Force 
        }
        else
        {
            $EC2Instance = New-EC2Instance -ImageId $ImageId -MinCount $MinCount -MaxCount $MaxCount `
                                           -SecurityGroup $SecurityGroup -SecurityGroupId $SecurityGroupId -KeyName $KeyName -UserData $UserData `
                                           -UserDataFile $UserDataFile -InstanceType $InstanceType -AvailabilityZone $AvailabilityZone `
                                           -PlacementGroup $PlacementGroup -Tenancy $Tenancy -KernelId $KernelId -RamdiskId $RamdiskId `
                                           -BlockDeviceMapping $BlockDeviceMapping -Monitoring_Enabled $Monitoring_Enabled -SubnetId $SubnetId `
                                           -DisableApiTermination $DisableApiTermination -InstanceInitiatedShutdownBehavior $InstanceInitiatedShutdownBehavior `
                                           -ClientToken $ClientToken -NetworkInterface $NetworkInterface `
                                           -EbsOptimized $EbsOptimized -InstanceProfile_Arn $InstanceProfile_Arn -InstanceProfile_Name $InstanceProfile_Name `
                                           -AdditionalInfo $AdditionalInfo 
        }
    }

    # Set tags
    for ( $idx = 0; $idx -lt $Tags.Length; $idx++ )
    {
        if ( $null -ne $Tags[$idx].Val -and '' -ne $Tags[$idx].Val )
        {
            $Tag = New-Object Amazon.EC2.Model.Tag
            $Tag.Key = $Tags[$idx].Key
            $Tag.Value = $Tags[$idx].Val
            New-EC2Tag -Resource $EC2Instance.Instances.InstanceId -Tag $Tag
        }
    }
}
catch
{
    Write-Host "It failed to create EC2 instance."
    Write-Host $Error
    exit $FATAL
}
