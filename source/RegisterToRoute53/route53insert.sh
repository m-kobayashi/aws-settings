#!/bin/sh
#
# Script to automatically register the host to Route53 when you start the EC2 instance.
#
#      Create 2015-07-01 By Masato Kobayashi
#
#
#   Copyright (C) 2015 Masato Kobayashi. All rights reserved.
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


# Template file path
TMPLATE=/etc/aws/ddnsrecord.tmpl
# Get EC2 instance id.
INSTANCEID=`/usr/bin/curl -s http://169.254.169.254/latest/meta-data/instance-id`
# Get EC2 region name.
REGION=`/usr/bin/curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone | /bin/sed -e 's/.$//g'`
export AWS_DEFAULT_REGION=${REGION}
# Create a temporary file to store variables.
DOMAIN=`/bin/mktemp`
HOST=`/bin/mktemp`
TICKET=`/bin/mktemp`
OWNER=`/bin/mktemp`
PROJECT=`/bin/mktemp`
STACK=`/bin/mktemp`
# Get the tag of EC2 instance.
/usr/bin/aws --output text ec2 describe-instances --filters Name=instance-id,Values="${INSTANCEID}" | /bin/grep 'TAGS' |
while read LINE
do
    TAG=`/bin/echo ${LINE} | /bin/cut -d ' ' -f2`
    VAL=`/bin/echo ${LINE} | /bin/cut -d ' ' -f3`
    case ${TAG} in
        'domain' )
            /bin/echo ${VAL} > ${DOMAIN}
            ;;
        'host' )
            /bin/echo ${VAL} > ${HOST}
            ;;
        'ticket' )
            /bin/echo ${VAL} > ${TICKET}
            ;;
        'owner' )
            /bin/echo ${VAL} > ${OWNER}
            ;;
        'project' )
            /bin/echo ${VAL} > ${PROJECT}
            ;;
        'stack' )
            /bin/echo ${VAL} > ${STACK}
            ;;
    esac
done
# To capture information that is written to a temporary file in a variable.
TMPFILE=${DOMAIN}
DOMAIN=`/bin/cat ${DOMAIN}`
/bin/rm -f ${TMPFILE}
TMPFILE=${HOST}
HOST=`/bin/cat ${HOST}`
/bin/rm -f ${TMPFILE}
TMPFILE=${TICKET}
TICKET=`/bin/cat ${TICKET}`
/bin/rm -f ${TMPFILE}
TMPFILE=${OWNER}
OWNER=`/bin/cat ${OWNER}`
/bin/rm -f ${TMPFILE}
TMPFILE=${PROJECT}
PROJECT=`/bin/cat ${PROJECT}`
/bin/rm -f ${TMPFILE}
TMPFILE=${STACK}
STACK=`/bin/cat ${STACK}`
/bin/rm -f ${TMPFILE}
# Executable judgment.
if [ -z "${DOMAIN}" ]; then
    echo "No domain."
    exit 1
fi
if [ -z  "${HOST}" ];  then
    echo "No host."
    exit 1
fi
if [ -z "${STACK}" ];  then
    echo "No stack."
    exit 1
fi
if [ `/bin/echo ${STACK} | /usr/bin/tr [a-z] [A-Z]` = 'PRODUCT' ];  then
    echo "This is product instance."
    exit 0
fi
# You get the host zone ID of the specified domain.
DOMAIN_P=`/bin/echo ${DOMAIN} | /bin/sed -e "s/\./\\./g"`
ZONEID=`/usr/bin/aws --output text route53 list-hosted-zones | /bin/grep 'hostedzone' | /bin/grep "${DOMAIN_P}\." | /bin/cut -f3 | /bin/cut -d '/' -f3`
# DNS record date file path
DNSRECD=`/bin/mktemp`
# Synthesize the host name.
if [ -n "${OWNER}" ];   then
    HOST="${OWNER}-${HOST}"
fi
if [ -n "${PROJECT}" ]; then
    HOST="${PROJECT}-${HOST}"
fi
if [ -n "${TICKET}" ];  then
    HOST="${HOST}-${TICKET}"
fi
# Get EC2 public ip address.
PUBLICIP=`/usr/bin/curl -s http://169.254.169.254/latest/meta-data/public-ipv4`
# You want to create a DNS record information.
/bin/sed -e "s/{%PUBLICIP%}/${PUBLICIP}/g;s/{%HOST%}/${HOST}/g;s/{%DOMAIN%}/${DOMAIN}/g" ${TMPLATE} > ${DNSRECD}
# Registered in the Route53.
/usr/bin/aws route53 change-resource-record-sets --hosted-zone-id ${ZONEID} --change-batch file://${DNSRECD}
/bin/rm -f ${DNSRECD}
exit 0
